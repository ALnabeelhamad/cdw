#!/bin/bash

yum -y install epel-release
yum -y install ansible git

chmod 777 /srv/ansible/ansible.cfg
sleep 5
ansible-playbook -i /srv/ansible/vagrant /srv/ansible/site.yml

# add the inventory file to the default location
cp -f /vagrant/files/host /etc/ansible/hosts
