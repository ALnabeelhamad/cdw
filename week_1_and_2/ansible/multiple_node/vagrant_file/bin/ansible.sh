#!/bin/bash

vagrant vbguest

yum -y install epel-release
yum -y install ansible

# add the inventory file to the default location
cp -f /vagrant/files/host /etc/ansible/host

Running ansible
chmod 777 /srv/ansible/ansible.cfg
sleep 5
ansible-playbook -i /srv/ansible/host /srv/ansible/site.yml
