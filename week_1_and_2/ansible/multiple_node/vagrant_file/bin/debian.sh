#!/bin/bash

vagrant vbguest

sudo su
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list

sudo apt-get -y update
sudo apt-get -y --force-yes install ansible
