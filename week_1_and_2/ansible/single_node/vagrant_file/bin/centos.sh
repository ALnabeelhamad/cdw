#!/bin/bash

vagrant vbguest
yum -y install epel-release
yum -y install ansible git

# add the inventory file to the default location
cp -f /vagrant/files/host /etc/ansible/host

chmod 777 /srv/centos/ansible.cfg
sleep 5
ansible-playbook -i /srv/centos/host /srv/centos/site.yml
