#!/bin/bash

vagrant vbguest

sudo su
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" >> /etc/apt/sources.list

sudo apt-get -y update
sudo apt-get -y --force-yes install ansible

# add the inventory file to the default location
cp -f /vagrant/files/host /etc/ansible/hosts

chmod 777 /srv/debian/ansible.cfg
sleep 5
ansible-playbook -i /srv/debian/host /srv/debian/site.yml
