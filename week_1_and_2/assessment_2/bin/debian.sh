#!/bin/bash

# install apache on (debian)
apt-get -y update
apt-get -y install apache2

# pass through my html page
cp -f /vagrant/file/webpage /etc/init.d
chmod a+x /etc/init.d/webpage

# Enable then start the webpage
systemctl enable webpage
systemctl start webpage

# start apache2
systemctl start apache2

# enable apache2
systemctl enable apache2

# just to make sure you have provisioned
echo "Debian provison has completed"
