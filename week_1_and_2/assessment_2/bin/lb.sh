#!/bin/bash

# install apache on (haproxy)
yum -y install haproxy

# pass through my haproxy config page
cp -f /vagrant/file/haproxy.cfg /etc/haproxy/haproxy.cfg

# start haproxy
systemctl start haproxy

# enable haproxy
systemctl enable haproxy

# just to make sure you have provisioned
echo "HA-Proxy provison has completed"
