# install apache on (rhel/centos)
yum -y install httpd

# pass through my html page
cp -f /vagrant/file/webpage /etc/init.d
chmod a+x /etc/init.d/webpage

# enable and start webpage
systemctl enable webpage
systemctl start webpage

# enable and start apache
systemctl start httpd
systemctl enable httpd


# just to make sure you have provisioned
echo "RHEL provison has completed"
