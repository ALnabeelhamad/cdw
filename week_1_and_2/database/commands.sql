select avg(price) from titles;
select avg(price) from titles group by type;
select avg(price), title from titles group by type;
select avg(price), type from titles group by type;
select sum(price) from titles;
select sum(price), title from titles group by type;
select sum(price), type from titles group by type;
select min(price), title, type from titles;
select count(title) from titles;
select sum(price), type from titles group by type having sum(price) > '40';
