create database if not exists stevedb;

-- drop user 'stevedb'@'%';
create user 'stevedb'@'%' identified by 'secret';
grant all privileges on stevedb.* to 'stevedb'@'%';
grant all privileges on stevedb.* to 'stevedb'@'localhost' identified by 'secret';

use stevedb;

create table if not exists contacts (
  id int not null auto_increment,
  firstname varchar(100) not null,
  lastname varchar(100) not null,
  phone varchar(30),
  email varchar(200),
  primary key (id)
);

create table if not exists contactdetail (
  contact_id int not null,
  photo blob,
  blurb text,
  foreign key (contact_id) references contacts(id),
  primary key (contact_id)
);

-- Inserting the data into the tables
insert into contacts (firstname,lastname,phone,email)
values('Steve','Shilling','555-1234','steve.shilling@automationlogic.com');
insert into contactdetail (contact_id,blurb)
values(LAST_INSERT_ID(),'This should be Steves info');

insert into contacts (firstname,lastname,phone,email)
values('Kris','Saxton','555-9999','kris@automatinologic.com');
insert into contactdetail (contact_id,blurb)
values(LAST_INSERT_ID(),'This should be Kris data');
