#!/bin/bash

# Ref: https://www.linode.com/docs/databases/postgresql/how-to-install-postgresql-relational-databases-on-centos-7/

# Download Postgress Yum Repository
wget https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm

# Install the RPM, as well as the EPEL repositories (install dependencies)
sudo yum -y install pgdg-centos96-9.6-3.noarch.rpm epel-release

# Update Yum and install Repository
sudo yum -y update
sudo yum -y install postgresql96-server postgresql96-contrib

# Initialise and Start database
sudo /usr/pgsql-9.6/bin/postgresql96-setup initdb
sudo systemctl start postgresql-9.6

# optional: Configure PostgreSQL to start on boot
sudo systemctl enable postgresql-9.6

# add database
sudo -u postgres psql </vagrant/file/mydbp


# login using: sudo -u postgres psql
