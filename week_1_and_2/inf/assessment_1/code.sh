#!/bin/bash

echo "> Creating your AWS Infrastructure"

######### INFRA #########

# Creating a VPC
vpcID=$(aws ec2 create-vpc --cidr-block 10.0.0.0/16 --output json | jq -r '.Vpc.VpcId')
aws ec2 create-tags --resources $vpcID --tags Key=Name,Value=Ntest

# Creating public subnet
pub_subID=$(aws ec2 create-subnet --vpc-id $vpcID --cidr-block 10.0.1.0/24 --output json | jq -r '.Subnet.SubnetId')
aws ec2 create-tags --resources $pub_subID --tags Key=Name,Value=Ntest_pub

# Creating private subnet
prv_subID=$(aws ec2 create-subnet --vpc-id $vpcID --cidr-block 10.0.0.0/24 --output json | jq -r '.Subnet.SubnetId')
aws ec2 create-tags --resources $prv_subID --tags Key=Name,Value=Ntest_prv


######### Public Subnet #########

# Creating internate gateway (IGW) for the VPC
igw=$(aws ec2 create-internet-gateway | jq -r '.InternetGateway.InternetGatewayId')
aws ec2 create-tags --resources $igw --tags Key=Name,Value=Ntest_igw

# Attach IGW to VPC
aws ec2 attach-internet-gateway --vpc-id $vpcID --internet-gateway-id $igw

# Creating a custom route table for the VPC
rt=$(aws ec2 create-route-table --vpc-id $vpcID --output json | jq -r '.RouteTable.RouteTableId')
aws ec2 create-tags --resources $rt --tags Key=Name,Value=Ntest_rt

# Route table routes
aws ec2 create-route --route-table-id $rt --destination-cidr-block 86.16.202.40/32 --gateway-id $igw --output json 1>/dev/null

# associate rtb to particular subnets
aws ec2 associate-route-table  --subnet-id $pub_subID --route-table-id $rt &>/dev/null

# Give public IP when instances launch in this public subnet
aws ec2 modify-subnet-attribute --subnet-id $pub_subID --map-public-ip-on-launch


######### Private Subnet #########

# Creating elastic IP for the NAT
e_IP=$(aws ec2 allocate-address --domain vpc --output json | jq -r '.AllocationId')

# Creating NAT gateway
nat=$(aws ec2 create-nat-gateway --subnet-id $prv_subID --allocation-id $e_IP --output json | jq -r '.NatGateway.NatGatewayId')
aws ec2 create-tags --resources $nat --tags Key=Name,Value=NAT

# Waiting for NAT gatwway to be created
waiting_nat=$(aws ec2 describe-nat-gateways --nat-gateway-ids $nat --output json | jq -r '.NatGateways[].State')

echo "> Creating NAT gw"

until [ "$waiting_nat" = "available" ]
do
  waiting_nat=$(aws ec2 describe-nat-gateways --nat-gateway-ids $nat --output json | jq -r '.NatGateways[].State')
  printf "."
  sleep 2
done

echo "> finished creating NAT gw"

# Create a Private route to NAT gw
aws ec2 create-route --route-table-id $rt --destination-cidr-block 0.0.0.0/0 --nat-gateway-id $nat --output json 1>/dev/null

# Associate a private channel in the route table
aws ec2 associate-route-table  --subnet-id $prv_subID --route-table-id $rt  &>/dev/null

#### SECURITY GROUPS #####

#~~~~ SSH ~~~~~#
# Public SSH
pub_ssh=$(aws ec2 create-security-group --group-name pub_ssh --description "pub_ssh" --vpc-id $vpcID --output json | jq -r '.GroupId')
aws ec2 create-tags --resources $pub_ssh --tags Key=Name,Value=prv_ssh

aws ec2 authorize-security-group-ingress \
    --group-id $pub_ssh \
    --protocol tcp \
    --port 22 \
    --cidr 0.0.0.0/0

# Private SSH
prv_ssh=$(aws ec2 create-security-group --group-name prv_ssh --description "prv_ssh" --vpc-id $vpcID --output json | jq -r '.GroupId')
aws ec2 create-tags --resources $prv_ssh --tags Key=Name,Value=prv_ssh

aws ec2 authorize-security-group-ingress \
    --group-id $prv_ssh \
    --protocol tcp \
    --port 22 \
    --cidr 86.16.202.40/32

#~~~~ HTTP ~~~~~#
# Public http
pub_http=$(aws ec2 create-security-group --group-name pub_http --description "pub_http" --vpc-id $vpcID --output json | jq -r '.GroupId')
aws ec2 create-tags --resources $pub_http --tags Key=Name,Value=pub_http

aws ec2 authorize-security-group-ingress \
    --group-id $pub_http \
    --protocol tcp \
    --port 80 \
    --cidr 0.0.0.0/0

# Private http
prv_http=$(aws ec2 create-security-group --group-name prv_http --description "prv_http" --vpc-id $vpcID --output json | jq -r '.GroupId')
aws ec2 create-tags --resources $prv_http --tags Key=Name,Value=prv_http

aws ec2 authorize-security-group-ingress \
    --group-id $prv_http \
    --protocol tcp \
    --port 80 \
    --cidr 86.16.202.40/32

####### Boxes/Machines ######
echo "creating instances"

# -- Jump box
jump=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $pub_ssh \
    --subnet-id $pub_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $jump --tags Key=Name,Value=jump

# -- Ansible
ansible=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $pub_http \
    --subnet-id $prv_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $ansible --tags Key=Name,Value=ansible

# ~~~ Apache model
# --- LB
apache_lb=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $pub_http \
    --subnet-id $pub_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $apache_lb --tags Key=Name,Value=apache_lb

# --- web1
apache_web1=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $prv_http \
    --subnet-id $prv_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $apache_web1 --tags Key=Name,Value=apache_web1

# --- web2
apache_web2=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $prv_http \
    --subnet-id $prv_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $apache_web2 --tags Key=Name,Value=apache_web2

# ~~~ Nginx model
# --- LB
nginx_lb=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $pub_http \
    --subnet-id $pub_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $nginx_lb --tags Key=Name,Value=nginx_lb

# --- web1
nginx_web1=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $prv_http \
    --subnet-id $prv_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $nginx_web1 --tags Key=Name,Value=nginx_web1

# --- web2
nginx_web2=$(aws ec2 run-instances \
    --image-id ami-03b4b78aae82b30f1 \
    --count 1 \
    --instance-type t2.micro \
    --key-name nabeel \
    --security-group-ids $prv_ssh $prv_http \
    --subnet-id $prv_subID | jq -r '.Instances[].InstanceId')

aws ec2 create-tags --resources $nginx_web2 --tags Key=Name,Value=nginx_web2

echo "> infrustructure has been created"
echo "> we are not moving into provisioning"

# Destroy script
cat >destroy.sh <<HERE
#!/bin/bash

echo "> Would you like to delete your virtual infrustructure? :"
read input

if [[ "$input" = [Yy]es ]]
then
  echo "deleting instances"
  aws ec2 terminate-instances --instance-ids $jump $ansible $apache_lb $apache_web1 $apache_web2 $nginx_lb $nginx_web1 $nginx_web2
  echo "deleting your security groups"
  aws ec2 delete-security-group --group-id $pub_ssh
  aws ec2 delete-security-group --group-id $prv_ssh
  aws ec2 delete-security-group --group-id $pub_http
  aws ec2 delete-security-group --group-id $prv_https
  echo "deleting NAT"
  aws ec2 delete-nat-gateway --nat-gateway-id $nat
  deleting_nat=$(aws ec2 describe-nat-gateways --nat-gateway-ids $nat --output json | jq -r '.NatGateways[].State')
  until [ "$deleting_nat" = "deleted" ]
  do
    waiting_nat=$(aws ec2 describe-nat-gateways --nat-gateway-ids $nat --output json | jq -r '.NatGateways[].State')
    printf "."
    sleep 2
  done

  echo "Delete your Internet gateway"
  aws ec2 delete-internet-gateway --internet-gateway-id $igw

  echo "deleting your subnets"
  aws ec2 delete-subnet --subnet-id $pub_subID
  aws ec2 delete-subnet --subnet-id $prv_subID
  echo "Delete your custom route table"
  aws ec2 delete-route-table --route-table-id $rt
  echo "Detach your Internet gateway from your VPC"
  aws ec2 detach-internet-gateway --internet-gateway-id $igw --vpc-id $vpcID
  echo "deleting vpc"
  aws ec2 delete-vpc --vpc-id $vpcID
fi

HERE

chmod 755 destroy.sh
