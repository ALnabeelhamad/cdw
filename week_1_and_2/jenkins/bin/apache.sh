#!/bin/bash

# updating the ubuntu packages
sudo apt update

# install apache on (rhel/centos)
apt-get -y install apache2

# pass through my html page
cp /vagrant/file/index.html /var/www/html

# start apache
service apache2 start

# enable apache
service apache2 enable

# just to make sure you have provisioned
echo "your apache provison has completed"
