#!/bin/bash

# updating vagrant
vagrant box update

# installing java
sudo apt update
sudo add-apt-repository ppa:openjdk-r/ppa -y
sudo apt-get update -y
sudo apt-get install openjdk-8-jdk -y
    # show java version
    java -version

# installing jenkins
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update -y
sudo apt-get install jenkins -y


# updating config file from https to http
cp /vagrant/file/hudson.model.UpdateCenter.xml /var/lib/jenkins/hudson.model.UpdateCenter.xml

# starting jenkins
sudo service jenkins start
sudo service jenkins enable


# provisioning of jenkin
    # Waiting for jenkins to start
    until cat /var/log/jenkins/jenkins.log | grep  "INFO: Started @"
    do
      echo -n "."
      sleep 2
    done

    # Waiting for the inital admin password to exit
    until ls /var/lib/jenkins/secrets/initialAdminPassword 2>&1
    do
      echo -n "."
      sleep 2
    done

    cat /var/lib/jenkins/secrets/initialAdminPassword

    sudo service jenkins stop

    # Waiting for admin file to exist
    until ls /var/lib/jenkins/users 2>&1
    do
      echo -n "."
      sleep 2
    done


    # extracting admin username
    init_admin=$(ls /var/lib/jenkins/users | grep 'admin')

    echo $init_admin

    # # copy users who'll have access to jenkins
    # sudo cp /vagrant/file/users.xml /var/lib/jenkins/users/users.xml

    # copy admin credential file
    sudo cp -f /vagrant/file/config.xml /var/lib/jenkins/users/$init_admin/config.xml

    sudo cp -f /vagrant/file/config.xml /home/vagrant

    ls /var/lib/jenkins/users | grep 'nabeel'

    sudo service jenkins start

    until cat /var/log/jenkins/jenkins.log | grep  "INFO: Started @"
    do
      echo -n "."
      sleep 2
    done
