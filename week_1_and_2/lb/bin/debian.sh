#!/bin/bash

# install apache on (debian)
apt-get -y update
apt-get -y install apache2

# pass through my html page
cp -f /vagrant/file/index.deb /var/www/html/index.html

# start apache2
systemctl start apache2

# enable apache2
systemctl enable apache2

# just to make sure you have provisioned
echo "Debian provison has completed"
