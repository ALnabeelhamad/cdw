#!/bin/bash

# install apache on (rhel/centos)
yum -y install httpd

# pass through my html page
cp -f /vagrant/file/index.rhel /var/www/html/index.html

# start apache
systemctl start httpd

# enable apache
systemctl enable httpd

# just to make sure you have provisioned
echo "RHEL provison has completed"
