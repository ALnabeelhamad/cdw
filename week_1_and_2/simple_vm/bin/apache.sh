#!/bin/bash

vagrant plugin install vagrant-vbguest

# install apache on (rhel/centos)
yum -y install httpd

# pass through my html page
cp /vagrant/file/index.html /var/www/html

# start apache
systemctl start httpd

# enable apache
systemctl enable httpd

# just to make sure you have provisioned
echo "your provison has completed"
