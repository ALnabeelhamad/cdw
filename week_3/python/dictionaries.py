#!/usr/local/bin/python3

# Creating a dictionary:
d = {}

# Ways of filling the direction
## Method 1  {key: value}
d = {"Nabeel": 27, "Seif": 37, "Mum": 61}
print(d)

## Method 2
d["Nabeel"] = 27
d["Seif"] = 37
d["Mum"] = 61
print(d)

# To find a specific key's value
print (d["Nabeel"])

# Update a value associated with a key
d["Seif"] = 38
print (d["Seif"])

# Note: Keys can be STRINGS or NUMBERS only
# Note: Values can be ANYTHING e.g
d[100] = 100
print(d)


# HOW TO ITERATE OVER KEY-VALUE PAIRS

for key, value in d.items():
    print ("key:")
    print (key)
    print ("value:")
    print (value)
    print ("")
