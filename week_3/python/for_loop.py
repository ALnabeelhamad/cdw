#!/usr/bin/python3

fruits = ["banana", "apple", "microsoft"]

print (fruits) # Shows/Does everything at once

for each_element in fruits: # Shows/Does each item in a list one by one
    print (each_element)


numbers = [1, 2, 3, 4]
for each_element in numbers:
    print (each_element)


# finding the total of each element in a list
total = 0
for each_element in numbers:
    total = total + each_element

print (total)

## Complex but beautiful method
total = 0       # you have to define it for the initial loop or the application will complain
for each_element in numbers:
    total = total + each_element  # i.e total += each_element
    print ("This is the current loop sum: %i" %total)

print ("This is the final total: %i" %total)


## Simpler
# This method only works when numbers are consecutative i.e. 1,2,3,4
a = list( range(1, 5) )  # this will show the numbers from 1 - 4
print (a)

total2 = 0
for each_element in range(1, 5):
    total2 = total2 + each_element

print (total2)

# Add values only a factor of 3 between 1 - 7
## print (list(range(1, 8))) = [1, 2, 3, 4, 5, 6, 7]

total3 = 0
for each_element in list(range(1, 8)):
    if each_element % 3 == 0:
        total3 = total3 + each_element

print (total3)  # 3+6=9

# EXERCISE
## add the factors of 3 and 5 less than 100

total4 = 0
for each_element in list(range(1,100)):
    if each_element % 3 == 0 or each_element % 5 == 0:
        total4 = total4 + each_element

print (total4)


##################################################
print ("::: class 2 :::")

a = ["apple", "banana", "republic"]

print ("METHOD 1")
for each_element in a:
    print (each_element)

# another way of doing this
print ("METHOD 2")
for i in range(0, 3):    # aka range(3) = 0, 1, 2
    for j in range(i + 1):
        # when i = 0 --> j = 0          i.e. when i = 0 going through j loop once ==> print apple once
        # when i = 1 --> j = 0, 1       i.e. when i = 1 going through j loop twice ==> print banana twice
        # when i = 2 --> j = 0, 1, 2    i.e. when i = 1 going through j loop 3times ==> print republic 3times
        print (a[i])
