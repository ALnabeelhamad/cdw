#!/usr/bin/python3

import sys

## FUNCTION ###
# AS A ....

# SET / COLLECTION OF CODE
def function1():
    print ("Hello, this is a function")

    # Calling the same function twice
function1()
function1()

# example 2
def function5(argument):
    print (argument)
    print ("yolo")

function5(3)

# MAPPING
    # Input or an argument e.g. x
def function2(x):
    return 2*x

    # Return value or output
    ## Should be 4
a = function2(2)
print (a)
    ## Should be 10
b = function2(5)
print (b)

    # Multiple inputs / arguments
def function3(x, y):
    return x + y

c = function3(6, 6)
print (c)


# COMBINING SET & MAPPING FUNCTION TOGETHER
def function4(z):
    print ("The value is %i" %z)
    print ("Still inside function 4")
    return 3 * z

d = function4(4)
# result will: 4  AND Still inside function 4
print (d)
# result will: 12


# EXERCISE BMI calculator
name1 = "NH"
height_m1 = 1.72
weight_kg1 = 85

name2 = "NH brother"
height_m2 = 1.75
weight_kg2 = 90

name3 = "NH sister"
height_m3 = 1.69
weight_kg3 = 70

print ("Welcome to Nabeel's very own BMI calcualtor")

def bmi_calculator(name, height_m, weight_kg):
    bmi = weight_kg / (height_m ** 2)
    print ("%s BMI result:" %name)
    print (bmi)
    if bmi < 25:
        return name + " is not overweight"
    else:
        return name + " is overweight"

result1 = bmi_calculator(name1, height_m1, weight_kg1)
print (result1)

result2 = bmi_calculator(name2, height_m2, weight_kg2)
print (result2)

result3 = bmi_calculator(name3, height_m3, weight_kg3)
print (result3)


# Exercise 2 - Converting miles to kilometers
print ("Select how many miles to convert to kilometers")
distance_miles = 2

print ("We are converting your miles to kilometers: ")
def distance_convertor(miles):
    km = 1.6 * miles
    return km

result4 = distance_convertor(distance_miles)
print (result4)
