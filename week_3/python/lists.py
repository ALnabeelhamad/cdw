#!/usr/bin/python3

# Simple list
a = [1, 2, 3, 4]
print (a)

# Add to a list
a.append(5)
print (a)

# Add string to a number lsit
a.append("hello nabeel")
print (a)

# Add a list onto an existing list
a.append(["is", "number" , "1"])
print (a)

# Delete the last item added to the list
a.pop() # deletes ["is", "number" , "1"]
print (a)
a.pop() # deletes hello nabeel
print (a)

# Retrieve from a list (e.g. first item)
print (a[0]) # prints first item i.e. 1
print (a[1]) # prints second item i.e. 2
print (a[-1]) # prints last item i.e. 5

# Changing a value in a list
a[0] = 0    # by assigning it a new value
print (a)   # first value now changed to 0

# Exercice
# swap the first and last item on the list without having to define them.

b = ["banana", "apple", "microsoft"]
temp = b[0]
b[0] = b[2]
b[2] = temp
print (b)

# shortcut of swapping - beauty of python
b[0], b[2] = b[2], b[0]
print (b) 
