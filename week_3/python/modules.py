#!/usr/local/bin/python3

import json
import requests
import sys

# RESOURCE MODULE
## User machine
resource=requests.post("http://headers.jsontest.com/") #User-Agent
assert resource.status_code == 200
data=resource.json()
print ("Your are:",data["User-Agent"])

## Users ip address
resource=requests.post("http://ip.jsontest.com/") #User-Agent
assert resource.status_code == 200
data=resource.json()
print ("Your ip address is:",data["ip"])

# SYS MODULE
## aguments i.e. $1, $2 ...etc
for i in range(len(sys.argv)):
    if i == 0:
        print ("Function name: %s" % sys.argv[0])
    else:
        print ("%d. argument: %s" % (i,sys.argv[i]))
