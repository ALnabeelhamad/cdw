#!/usr/local/bin/python3

# list(range(1, 5)) = [1, 2, 3, 4]
# show the sum of 1 until 4
total = 0
for each_element in list(range(1, 5)):
    total = total + each_element
print(total)

# Another method of doing the same operation below is using a while loop
total = 0
x = 1
while x < 5:
    total = total + x
    x = x + 1
print(total)

# Show the sum of only the positive numbers
list = [5, 4, 4, 3, 1, -2, -3, -5]

total2 = 0
x = 0
while list[x] > 0:
    total2 = total2 + list[x]
    x = x + 1

print(total2)

# you need to make sure to include a parameter limiting the test not to exceed the list in case
# the list is just full of positive numbers i.e. [1 ,2, 3] the whilst list[x] > 0 will fail as it will try
# to run the non exitant 3 value i.e. what would be 4 (1, 2, 3, [4])
list = [5, 4, 4, 3, 1]
print ( len(list) ) # =5

total2 = 0
x = 0
while x < len(list) and list[x] > 0:
    total2 = total2 + list[x]
    x = x + 1

print(total2)


# BREAK STATEMENT
list = [5, 4, 4, 3, 1, -2, -3, -5]

total3 = 0
for each_element in list:
    if each_element < 0:
        break
    total3 = total3 + each_element

print(total3)

# If the list required sorting (descending order)
list_2 = [5, 4, 4, -2, -3, -5, 3, 1]
list_2.sort(reverse = True)

total4 = 0
for each_element in list_2:
    if each_element < 0:
        break
    total4 = total4 + each_element

print(total4)

# Break for while loop
list_2 = [5, 4, 4, 3, 1, -2, -3, -5]

total5 = 0
x = 0
while True:     # i.e. always run this while loop. It's like saying while 1 < 2 i.e. it's always going to be True
    total5 = total5 + list_2[x]
    x = x + 1
    if list_2[x] < 0:
        break

print (total5)


# EXERCISE - find the sum of all the negative numbers
print ("Compeleting the EXERCISE")
given_list = [7, 5, 4, 4, 3, 1, -2, -3, -5, -7]

total = 0
x = 0

while x < len(given_list):
    if given_list[x] < 0:  # started of with given_list[6] to check how the if statement works
        total = total + given_list[x]   # started of with print(6) to see what gets printed out
    x = x + 1   # this is outside the if statement because I need the computer to go through each item on the list to check if it's a negative value

print (total)

total = 0
x = 0
for each_element in given_list:
    if given_list[x] < 0:
        total = total + given_list[x]
    x = x + 1

print (total)
